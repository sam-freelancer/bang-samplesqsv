/**
 *  Navigation Routes defind
 */
import { createStackNavigator } from 'react-navigation-stack';


import SplashScreen from '../containers/SplashScreen/SplashScreen';
import SwiperSlideScreen from '../containers/SwiperScreen/SwiperScreen';
import WelcomeScreen from '../containers/WelcomeScreen/WelcomeScreen';

const AppNavigation = createStackNavigator(
  {
    Splash: {
      screen: SplashScreen
    },
    Welcome: {
      screen: WelcomeScreen
    },

    SwiperScreen: {
      screen: SwiperSlideScreen
    },
  },
  {
    initialRouteName: 'Welcome',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default AppNavigation;
