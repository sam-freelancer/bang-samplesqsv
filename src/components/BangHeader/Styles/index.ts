import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../../theme';
import { Fonts } from '../../../theme';
export default EStyleSheet.create({
  defaultBtn: {
    flexDirection: 'row',
    width: '140rem',
    height: '45rem',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '25rem',
    alignSelf: 'center'
  },
  container:{
    flexDirection: 'row',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: "100%"
  } ,
  Callus:{
    justifyContent: 'flex-end',
    marginLeft: 'auto',
    alignSelf: 'flex-end'
  }
  ,
  rightBtn: {
    right: '40rem',
    tintColor: 'white',
    position: 'absolute'
  },
  tinyLogo: {
    marginLeft: '130rem',
    width: '99rem',
    height: '65rem',
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  titleTxtWhite: {
    fontSize: '18rem',
    ...Fonts.style.boldWhite,
    textAlign: 'center'
  },
});
