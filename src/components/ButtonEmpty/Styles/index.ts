import EStyleSheet from 'react-native-extended-stylesheet';
import { Colors } from '../../../theme';
import { Fonts } from '../../../theme';
export default EStyleSheet.create({
  defaultBtn: {
    flexDirection: 'row',
    width: '240rem',
    height: '45rem',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '10rem',
    borderColor:'#E1C77F',
    borderWidth:1,
   
    alignSelf: 'center'
  },
  bangButtonOutline: {

    color:'black',
    borderRadius: '10rem',
   
  },


  rightBtn: {
    right: '40rem',
    tintColor: 'white',
    position: 'absolute'
  },
  titleTxtWhite: {
    fontSize: '16rem',
    ...Fonts.style.regularWhite,
    textAlign: 'center'
  },
});
