import React from 'react';
import {
  Text,
  Image,
  TouchableOpacity as TouchableOpacityClass,
  View,
  Linking
} from 'react-native';
import styles from './Styles';
import { WithPreventDoubleClick } from './WithPreventDoubleClick';
import { Images, Colors } from './../../theme';
import LinearGradient from 'react-native-linear-gradient';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';

const TouchableOpacity = WithPreventDoubleClick(TouchableOpacityClass);

export interface Props {
  onPress: any;
  style?: any;
  titleStyle?: any;
  title: any;
  BangHeaderStyle?: any;
  isArrowBtnTrue?:any;
}



class BangHeader extends React.PureComponent<Props> {

  dialCall = (number) => {
    let phoneNumber = '';
    if (Platform.OS === 'android') { phoneNumber = `tel:${number}`; }
    else {phoneNumber = `telprompt:${number}`; }
    Linking.openURL(phoneNumber);
 };
  render() {
    const { onPress, title, titleStyle, BangHeaderStyle,isArrowBtnTrue } = this.props;

    return (
      <View style={styles.container}>
                <TouchableOpacityClass 
      style={styles.CallusbaCK}
      onPress={()=>{this.dialCall(123456789)}}
      
      >

 <Image
 source={require('./back.png')}
/>
        </TouchableOpacityClass>
   
      <Image
        style={styles.tinyLogo}
        source={require('./banglogo.png')}
      />

      <TouchableOpacityClass 
      style={styles.Callus} 
      onPress={()=>{this.dialCall(123456789)}}
      >

        <Image
        source={require('./Radial.png')}
        />
        </TouchableOpacityClass>


     

    </View>
    );
  }
}

export default BangHeader;
